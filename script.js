const makeList = (arrayList, domElement = document.body) => {
    const createUl = document.createElement("ul");
    domElement.append(createUl);
    arrayList.forEach(item => {
        const createLi = document.createElement("li");
        createUl.append(createLi);
        createLi.innerText = item;
    });
}
makeList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);